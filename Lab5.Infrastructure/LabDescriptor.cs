﻿using System;
using System.Reflection;
using PK.Container;
using Glowny.Interface;
using Wyswietlacz.Interface;


namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Kontener.Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny.Klasa.Glowny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz.Klasa.Wyswietlacz));

        #endregion
    }
}
