﻿using System;
using PK.Container;
using Wyswietlacz.Interface;
using Glowny.Interface;
using Kontener;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer kontener = new Container();

            IWyswietlacz wyswietlacz = new Wyswietlacz.Klasa.Wyswietlacz();
            IGlowny glowny = new Glowny.Klasa.Glowny(wyswietlacz);

            kontener.Register(wyswietlacz);
            kontener.Register(glowny);

            return kontener;
        }
    }
}
